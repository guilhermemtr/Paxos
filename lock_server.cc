// Lock server implementation

#include <sstream>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "rpc/jsl_log.h"
#include "rpc/slock.h"
#include "lock_server.h"

lock_state::lock_state():
  clt(-1), n_acquires(0), state(lock_state::FREE)
{
  assert(pthread_mutex_init(&mutex, NULL) == 0);
  assert(pthread_mutex_init(&marshal_mutex, NULL) == 0);
}

lock_state::~lock_state()
{
  assert(pthread_mutex_destroy(&mutex) == 0);
  assert(pthread_mutex_destroy(&marshal_mutex) == 0);
}

int
lock_state::get_acquires()
{
  return this->n_acquires;
}

lock_state::l_state
lock_state::acquire(int clt)
{
  ScopedLock ml(&mutex);
  while(state == LOCKED) {
    return LOCKED;
  }
  this->clt = clt;
  this->n_acquires++;
  this->state = LOCKED;
  return FREE;
}

lock_state::l_state
lock_state::release(int clt)
{
  ScopedLock ml(&mutex);
  if(clt != this->clt || state == FREE) {
    return FREE;
  }
  this->clt = -1;
  this->state = FREE;
  return LOCKED;
}

lock_server::lock_server(class rsm *_rsm):
rsm(_rsm)
{
  locks = std::map<lock_protocol::lockid_t, lock_state *>();
  request_history = std::map<lock_protocol::lockid_t, lock_server_request_id *>();
  pthread_mutex_init(&mutex, NULL);
  rsm->set_state_transfer(this);
}

lock_server::~lock_server()
{
  pthread_mutex_destroy(&mutex);
}

lock_protocol::status
lock_server::stat(int clt, lock_protocol::lockid_t lid, int &r)
{
  lock_state *l;
  {
    ScopedLock ml(&mutex);
    jsl_log(JSL_DBG_4, "stat request from clt %d\n", clt);
    if((l = locks[lid]) == NULL) {
      r = lock_protocol::NOENT;
      return lock_protocol::NOENT;
    }
  }
  r = l->get_acquires();
  return lock_protocol::OK;
}

lock_protocol::status
lock_server::acquire(int clt, lock_protocol::lockid_t lid, int seqno, int &r)
{
  lock_state *l;
  {
    ScopedLock ml(&mutex);
    jsl_log(JSL_DBG_4, "acquire request from clt %d\n", clt);
    if((l = locks[lid]) == NULL) {
      locks[lid] = l = new lock_state();
    }

    lock_server_request_id * lsreqid;
    //printf("lock_server::acquire: received acquire request from (%d,%d)\n", clt, seqno);
    if((lsreqid = request_history[lid]) != NULL) {
      //printf("lock_server::acquire: there is already a lock request from (%d,%d)\n", lsreqid->clt, lsreqid->seqno);
      if(lsreqid->clt == clt && lsreqid->seqno == seqno) {
        //printf("lock_server::acquire: repeated request: (%d,%d)\n", clt, seqno);
        r = lock_protocol::OK;
        return lock_protocol::OK;
      }
    }
  }
  lock_state::l_state s = l->acquire(clt);
  if(s == lock_state::FREE) {
    r = lock_protocol::OK;
    ScopedLock ml(&mutex);
    if(request_history[lid] != NULL) {
      delete request_history[lid];
      std::map<lock_protocol::lockid_t, lock_server_request_id *>::iterator iter_request_history = request_history.find(lid);
      request_history.erase(iter_request_history);
    }
    request_history[lid] =  new lock_server_request_id(clt, seqno);
    //printf("lock_server::acquire: granting lock %llu (%d,%d)\n", lid, clt, seqno);
  } else {
    //printf("lock_server::acquire: not granting lock %llu (%d,%d)\n", lid, clt, seqno);
    r = lock_protocol::RETRY;
  }
  return r;
}

lock_protocol::status
lock_server::release(int clt, lock_protocol::lockid_t lid, int &r)
{
  lock_state *l;
  {
    ScopedLock ml(&mutex);
    jsl_log(JSL_DBG_4, "release request from clt %d\n", clt);
    if((l = locks[lid]) == NULL) {
      r = lock_protocol::NOENT;
      return r;
    }
  }
  if(l->release(clt) == lock_state::LOCKED) {
    //printf("lock_server::release: releasing lock %llu\n", lid);
  } else {
    //printf("lock_server::release: not releasing lock %llu\n", lid);
  }
  r = lock_protocol::OK;
  return r;
}

std::string 
lock_server::marshal_state() {

  // lock any needed mutexes
  marshall rep;
  assert(pthread_mutex_lock(&mutex) == 0);
  rep << (unsigned int) locks.size();
  std::map<lock_protocol::lock_protocol::lockid_t, lock_state *>::iterator iter_lock;
  for (iter_lock = locks.begin(); iter_lock != locks.end(); iter_lock++) {
    lock_protocol::lockid_t lid = iter_lock->first;
    lock_state * stat = locks[lid];
    rep << lid;
    rep << (*stat);
  }

  rep << (unsigned int) request_history.size();
  std::map<lock_protocol::lockid_t, lock_server_request_id *>::iterator iter_request_history;
  for (iter_request_history = request_history.begin(); iter_request_history != request_history.end(); iter_request_history++) {
    lock_protocol::lockid_t lid = iter_request_history->first;
    lock_server_request_id * reqid = request_history[lid];
    rep << lid;
    rep << (*reqid);
  }

  assert(pthread_mutex_unlock(&mutex) == 0);

  // unlock any mutexes
  return rep.str();

}

void 
lock_server::unmarshal_state(std::string state) {

  // lock any needed mutexes
  unmarshall rep(state);
  assert(pthread_mutex_lock(&mutex) == 0);
  locks.clear();
  unsigned int locks_size;
  rep >> locks_size;
  for (unsigned int i = 0; i < locks_size; i++) {
    lock_protocol::lockid_t lid;
    rep >> lid;
    lock_state * stat = new lock_state();
    rep >> (*stat);
    locks[lid] = stat;
  }

  request_history.clear();
  unsigned int request_history_size;
  rep >> request_history_size;
  for(unsigned int i = 0; i < request_history_size; i++) {
    lock_protocol::lockid_t lid;
    lock_server_request_id * reqid = new lock_server_request_id();
    rep >> lid;
    rep >> (*reqid);
    request_history[lid] = reqid;
    
  }

  assert(pthread_mutex_unlock(&mutex) == 0);
  // unlock any mutexes
}